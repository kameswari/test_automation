package com.selenium.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.android.AndroidDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;




public class SeleniumObject extends TestBase{

	public static String browser = new String();
	public static WebDriver driver;
	public static WebElement wait;

	
	public static WebDriver getDriver()
	{	
		browser = TestBase.getBrowser();
		
		
		
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			
			/*ChromeOptions options = new ChromeOptions(); 
			options.addArguments("disable-infobars"); */
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("ie")) {
			
			System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		return driver;
	}
	

	
	
	
	public static void launchApplication(String url){
		System.out.println("url: " + ApplicationURL);
		driver.get(ApplicationURL);
		
	}
	
	
	public static void quitBrowser() throws InterruptedException  {
		Thread.sleep(9000);
		driver.quit();
	}



	public static void closeBrowser() throws InterruptedException {
		
		Thread.sleep(9000);
		driver.close();
		
	}



}
