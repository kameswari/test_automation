package com.logo.test;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.dataprovider.Personaldataprovider;
import com.automation.pages.CartSummaryPage;
import com.automation.pages.LogoPage;
import com.automation.pages.RegistrationPage;
import com.automation.pages.SelectItems;
import com.automation.util.TestInfo;
import com.selenium.utils.TestBase;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class LogoTest extends TestBase {
	
	Logger log	=	Logger.getLogger(LogoTest.class);
	public String email = TestInfo.getemail();
	@BeforeClass(alwaysRun = true)
	public void setup()  {
		TestBase.setUp();
		driver = TestBase.getDriver();
		ApplicationURL=TestBase.getUrl();
		TestBase.launchApplication(ApplicationURL);
				
		}
	
	
	@Test(enabled = true,dataProvider="Personaldata",dataProviderClass = Personaldataprovider.class,priority=0)
	@Severity(SeverityLevel.NORMAL)
	@Description("verify user signIn and register")
	public void account_SignIn_Register(String cfname,String clname,String fname,String lname,String company,String address1,String address2,String city,String zip,String additional_info,String pnumber,String mnumber,String alt_address) throws InterruptedException {
		
		
		LogoPage logoPage = new LogoPage();
		logoPage.clickOnSignIn();
        logoPage.createAccount_SignIn(email);
		RegistrationPage rp = new RegistrationPage();
		rp.createAccount(cfname,clname,fname,lname,company,address1,address2,city,zip,additional_info,pnumber,mnumber,alt_address);
		String username[] = rp.getuserAccount().split(" ");
		log.info("1.validating firstname");
		Assert.assertEquals(username[0], cfname);
		log.info("2.validating firstname");
		Assert.assertEquals(username[1], clname);
		logoPage.Logout();
	
	}
	
	@Test(enabled = true,priority=1)
	@Severity(SeverityLevel.NORMAL)
	@Description("verify user order summary")
	public void login_user() throws InterruptedException {
		
		LogoPage logoPage = new LogoPage();
		SelectItems si	=new SelectItems();
		CartSummaryPage csp	=new CartSummaryPage();
		logoPage.clickOnSignIn();
		logoPage.Login(email);
		si.select_category();
		si.addtoCart_item();
		csp.summary_TAB();
		csp.address_TAB();
		csp.shipping_TAB();
		csp.payment_TAB();
		
		
	}
	

}
