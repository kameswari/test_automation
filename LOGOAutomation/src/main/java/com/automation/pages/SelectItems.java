package com.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import com.selenium.utils.SeleniumFunctions;
import com.selenium.utils.TestBase;

import io.qameta.allure.Step;

public class SelectItems extends TestBase{
	
	
	static SeleniumFunctions sf;
	By women_menu = By.xpath("//a[@title='Women']");
	By tshirts_submenu =By.xpath("//a[@title='Women']//following :: a[2]");
	static By tshirt	=	By.xpath("//img[@title='Faded Short Sleeve T-shirts']");
	By add_to_cart = By.xpath("//a[@title='Add to cart']");
	By pro_checkout = By.xpath("//a[@title='Proceed to checkout']");
	 
	
    public SelectItems() {
    	TestBase.setUp();
    	sf = new SeleniumFunctions();
    	PageFactory.initElements(driver, this);

	}
    
    @Step("Verifying main menu and sub menu items")
    public void select_category()  {
    	
    	 sf.waitForTheElement(women_menu);
    	 Actions action = new Actions(driver);
    	 action.moveToElement(sf.locateElement(women_menu)).perform();
    	 sf.waitForTheElement(tshirts_submenu);
    	 action.moveToElement(driver.findElement(tshirts_submenu)).click().perform();
    	    	
    }
    
   
    @Step("Verifying adding item to cart and poceed to checkout")
    public void addtoCart_item()  {
    	sf.waitForTheElement(tshirt);
    	 Actions action = new Actions(driver);
    	 action.moveToElement(sf.locateElement(tshirt)).perform();
    	 sf.waitForTheElement(add_to_cart);
    	 action.moveToElement(driver.findElement(add_to_cart)).click().perform();
    	 sf.waitForTheElement(pro_checkout);
    	 sf.clickOnElement(pro_checkout);
    	    	
    }
}
