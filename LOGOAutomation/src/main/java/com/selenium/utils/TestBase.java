package com.selenium.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestBase {
	
	public static WebDriver driver;
	private static final String PROP_FILE = "testConfig.properties";
	public static Properties properties = new Properties();
	//public static String browser = new String();
	public static String SeleniumBrowser = null;
	public static String ApplicationURL = null;
	
	static {

		Map<Object, Object> map = System.getProperties();
		SeleniumBrowser = (String) map.get("browser");
		ApplicationURL = (String) map.get("application.url");
		
		
			}
	
	/**
	 * Method to read the test configuration properties file properties and load
	 * the variables
	 */
	public static void setUp() {
		try {
			FileInputStream in = new FileInputStream(PROP_FILE);
			properties.load(in);
			in.close();
		} catch (IOException e) {
			System.err.println("Failed to read from " + PROP_FILE + " file.");
		}
	}

	public static String getBrowser() 
	{
		return String.valueOf(properties.getProperty("browser")).trim();	
	}
	
	public static String getUrl() 
	{
		return String.valueOf(properties.getProperty("url")).trim();	
	}
	
	public static WebDriver getDriver()
	{	
		SeleniumBrowser = TestBase.getBrowser();
		
		
		
		if (SeleniumBrowser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			
			ChromeOptions options = new ChromeOptions(); 
			options.addArguments("disable-infobars"); 
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setAcceptInsecureCerts(true);
			//ChromeDriver(capabilities);
			driver = new ChromeDriver(capabilities);
		} else if (SeleniumBrowser.equalsIgnoreCase("ie")) {
			
			System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		return driver;
	}
	public static void launchApplication(String url){
		System.out.println("url: " + ApplicationURL);
		driver.get(ApplicationURL);
		
	}
	

		
	
  
}
