package com.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestInfo;
import com.selenium.utils.SeleniumFunctions;
import com.selenium.utils.TestBase;

public class RegistrationPage extends TestBase{
	
	SeleniumFunctions sf;
	By genderrb	=	By.id("id_gender2");
	By cfanmetb	=	By.id("customer_firstname");
	By clanmetb		=	By.id("customer_lastname");
	By emailtb	=	By.id("email");
	By pwdtb	=	By.id("passwd");
	By daydp	=	By.id("days");
	By monthsdp	=	By.id("months");
	By yearsdp	=	By.id("years");
	By newslettercb	=	By.id("newsletter");
	By optiontcb	=	By.id("optin");
	By fnametb	=	By.id("firstname");
	By lnametb	=	By.id("lastname");
	By companytb	=	By.id("company");
	By address1tb	=	By.id("address1");
	By address2tb	=	By.id("address2");
	By citytb	=	By.id("city");
	By statedp	=	By.id("id_state");
	By ziptb	=	By.id("postcode");
	By countrydp	=	By.id("id_country");
	By infotb	=	By.id("other");
	By phonetb	=	By.id("phone");
	By mobilephntb	=	By.id("phone_mobile");
	By aliastb	=	By.id("alias");
	By submitAccBtn	=	By.id("submitAccount");
	By cusname = By.xpath("//a[@title='View my customer account']//span");
	
    public RegistrationPage() {
    	TestBase.setUp();
    	sf = new SeleniumFunctions();
    	PageFactory.initElements(driver, this);

	}
    
    public void createAccount(String cfname,String clname,String fname,String lname,String company,String address1,String address2,String city,String zip,String additional_info,String pnumber,String mnumber,String alt_address)  {
    	
    	sf.waitForTheElement(genderrb);
    	sf.selectElement(genderrb,"MRS");
    	sf.enterText(cfanmetb, cfname);
    	sf.enterText(clanmetb, clname);
    	sf.enterText(pwdtb, TestInfo.getPassword());
    	sf.selectFromDropDown(daydp,"value","5");
    	sf.selectFromDropDown(monthsdp,"value","6");
    	sf.selectFromDropDown(yearsdp,"value","2000");
    	sf.selectElement(newslettercb, "newslettercheckbox");
    	sf.selectElement(optiontcb, "Receive special offers from our partners!");
    	sf.enterText(fnametb, fname);
    	sf.enterText(lnametb, lname);
    	sf.enterText(companytb, company);
    	sf.enterText(address1tb, address1);
    	sf.enterText(address2tb, address2);
    	sf.enterText(citytb, city);
    	sf.selectFromDropDown(statedp, "value", "2");
    	sf.enterText(ziptb, zip);
    	sf.enterText(infotb, additional_info);
    	sf.enterText(phonetb, pnumber);
    	sf.enterText(mobilephntb, mnumber);
    	sf.enterText(aliastb, alt_address);
    	sf.clickOnElement(submitAccBtn);
    	
    }
    
    public  String getuserAccount()  {
    	
    	sf.waitForTheElement(cusname);
    	String username =sf.getTextOfAnElement(cusname);
       	return username;
    }
    

}
