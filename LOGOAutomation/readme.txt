Git location : https://gitlab.com/kameswari/test_automation.git
command to clone project : git clone https://gitlab.com/kameswari/test_automation.git
command to run project from command prompt : 
mvn clean install
mvn test
commands to see reports:
brew install allure (to install allure)
allure serve allure-results (to see report)
Note: above command share allure report results link

Framework Details:
1.test class file LOGOAutomation\src\test\java\com\logo\test\LogoTest.java
2.page calsses LOGOAutomation\src\main\java\com\automation\pages
3.data provider LOGOAutomation\src\main\java\com\automation\dataprovider\Personaldataprovider.java
4.LOGOAutomation\src\main\java\com\selenium\utils contains handling webdriver,elements,browser 
5. Test data contains customer personal data
6. current code runs on chrome driver

* used page object model and hybrid framework (experienced into this framework)
* not handled internet explorer