package com.automation.util;
	
	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.IOException;
	import org.apache.poi.ss.usermodel.Cell;
	import org.apache.poi.ss.usermodel.CellType;
	import org.apache.poi.ss.usermodel.Sheet;
	import org.apache.poi.xssf.usermodel.XSSFWorkbook;



	public class TestUtil {

	
		public static String TESTDATA_SHEET_PATH = "./Testdata/customerdata.xlsx";
		static XSSFWorkbook book;
		static Sheet sheet;
		static String testcategory;
		
		public static Object[][] getTestData1(String sheetName) {
			FileInputStream file = null;

			try {
				file = new FileInputStream(TESTDATA_SHEET_PATH);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			try {

				book = new XSSFWorkbook(file);

			} catch (IOException e) {
				e.printStackTrace();
			}

			sheet = book.getSheet(sheetName);
			Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
			for (int i = 0; i < sheet.getLastRowNum(); i++) {

				for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {

					Cell cell = sheet.getRow(i + 1).getCell(j);

					if (null != cell ) {
						if(cell.getCellType() == CellType.NUMERIC) {
							data[i][j] =cell.getNumericCellValue();
						} else {
							data[i][j] = cell.getStringCellValue();
						}
					}
					System.out.println(data[i][j]);

				}
			}
			return data;

		}
					
	}
	
	

