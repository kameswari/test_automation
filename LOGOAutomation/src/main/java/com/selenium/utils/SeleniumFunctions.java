package com.selenium.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumFunctions extends TestBase {
	Logger log	=	Logger.getLogger(SeleniumFunctions.class);
	
	public SeleniumFunctions() {
		
		PageFactory.initElements(driver, this);
	}
	
	
	public void clickOnElement(By by) {
		try {
			waitForTheElement(by);
			driver.findElement(by).click();
		} catch (Exception e) {
			log.info(" Not able to click on the link");
		}
	}

	public void enterText(By by, String strValue) {
		try {

			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(strValue);
			driver.findElement(by).sendKeys(Keys.TAB);

		} catch (Exception e) {
			log.info(" field is not visible");
		}
	}
	
	public String getTextOfAnElement(By by) {
		String actualValue;
		
		actualValue = driver.findElement(by).getText();
		log.info("Actual Value: " + actualValue);
		return actualValue;
		
	}
	public boolean waitForTheElement(By by) {

		WebElement element = (new WebDriverWait(driver, 120)).until(ExpectedConditions.visibilityOfElementLocated(by));
		return element.isDisplayed();
		
	}

	public boolean selectElement(By by, String elementName) {
		

			if (!driver.findElement(by).isSelected()) {
				driver.findElement(by).click();
			} else {
				log.info(" checkbox/redio button already selected");
			}
			return driver.findElement(by).isSelected();
		}
		
	
	public void selectFromDropDown(By by, String type,String value) {
		
		Select select = new Select(driver.findElement(by));
		switch(type) {
		case "index":
			select.selectByIndex(Integer.parseInt(value));
			break;
		case "value":
			select.selectByValue(value);
			break;
		case "visibletext":
			select.selectByVisibleText(value);
			break;
		default:
			System.out.println("please provide valid value");
			
		}
	}
	
	public WebElement locateElement(By locator) {
		WebElement loc=	driver.findElement(locator);
		return loc;
	}
	
}
