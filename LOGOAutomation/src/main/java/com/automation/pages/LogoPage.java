package com.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestInfo;
import com.selenium.utils.SeleniumFunctions;
import com.selenium.utils.TestBase;

import io.qameta.allure.Step;
public class LogoPage extends TestBase{

	SeleniumFunctions sf;
	static String email;
	By signIn = 	By.className("login");
	By createAccount = By.xpath("//form[@id=\"create-account_form\"]//h3");
	By emailAddress	=	By.xpath("//label[contains(text(),'Email address')]");
	By emailTextBox	=	By.id("email_create");
	By submitBtn	=	By.id("SubmitCreate");
	By login_email	=	By.id("email");
	By login_pwd	=	By.id("passwd");
	By login_submit	=	By.id("SubmitLogin"); 
	By logoutBtn	=	By.className("logout");
	
	
    public LogoPage() {
    	TestBase.setUp();
    	sf = new SeleniumFunctions();
    	PageFactory.initElements(driver, this);

	}
    
    @Step("Verifying sign In link is displayed and able to click")
	public void clickOnSignIn()  {
		sf.clickOnElement(signIn);
		
	}
	
    @Step("creating account with email : {}")
	public void createAccount_SignIn(String email)  {
		sf.waitForTheElement(submitBtn);
		sf.enterText(emailTextBox,email );
		sf.clickOnElement(submitBtn);
		
	}
	
    @Step("login with email : {}")
	public void Login(String email)  {
		sf.waitForTheElement(login_submit);
		sf.enterText(login_email, email);
		sf.enterText(login_pwd, TestInfo.getPassword());
		sf.clickOnElement(login_submit);
		
	}
	
    @Step("verifying logout button is clickeable")
	public void Logout()  {
		sf.waitForTheElement(logoutBtn);
		sf.clickOnElement(logoutBtn);
		
	}
	
	
}
