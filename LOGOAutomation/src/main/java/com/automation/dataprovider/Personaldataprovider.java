package com.automation.dataprovider;

import org.testng.annotations.DataProvider;

import com.automation.util.TestUtil;

public class Personaldataprovider {
	
	@DataProvider(name="Personaldata")
	public Object[][] getTestData() {
		
		Object data[][] = TestUtil.getTestData1("data");
		return data;
	}

}
