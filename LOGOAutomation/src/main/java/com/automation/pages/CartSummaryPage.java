package com.automation.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.selenium.utils.SeleniumFunctions;
import com.selenium.utils.TestBase;

import io.qameta.allure.Step;

public class CartSummaryPage extends TestBase {
	
	Logger log	=	Logger.getLogger(CartSummaryPage.class);
	SeleniumFunctions sf;
	By summary = By.className("step_current  first");
	By address = By.className("step_current  third");
	By shipping = By.className("step_current  four");
	By payment = By.id("step_end");
	By summary_checkout = By.xpath("//p//a[@title='Proceed to checkout']");
	By address_checkout = By.xpath("//button[@type='submit' and @name='processAddress']");
	By shipping_checkout = By.xpath("//button[@type='submit' and @name='processCarrier']");
	By tnccb = By.id("cgv");
	By order_des = By.xpath("//a[contains(text(),'Faded Short Sleeve T-shirts')]");
	By tabledata = By.xpath("//table[@id='cart_summary']//tr");
	
   public CartSummaryPage() {
    	TestBase.setUp();
    	sf = new SeleniumFunctions();
    	PageFactory.initElements(driver, this);

	}
    
   @Step("checkout from summary tab")
    public void summary_TAB()  {
    	sf.waitForTheElement(summary_checkout);
    	sf.clickOnElement(summary_checkout);
    }
    
   @Step("checkout from address tab")
    public void address_TAB()  {
    	sf.waitForTheElement(address_checkout);
    	sf.clickOnElement(address_checkout);
    }
    
   @Step("checkout from shipping tab")
    public void shipping_TAB()  {
    	sf.waitForTheElement(shipping_checkout);
    	sf.selectElement(tnccb,"Terms of service checkbox");
    	sf.clickOnElement(shipping_checkout);
    }
   
   @Step("validating payment info")
    public void payment_TAB()  {
    	
    	sf.waitForTheElement(tabledata);
       	List<WebElement> tcdatcdatata =  driver.findElements(tabledata);
    	for(int i=0;i<tcdatcdatata.size();i++) {
    		
    		String s=tcdatcdatata.get(i).getText();
    		if(s.contains("Total products")) {
    			log.info("verifying Total products");
    			Assert.assertEquals(s,"Total products $16.51");
    		} else if(s.contains("Total shipping ")) {
    			log.info("verifying Total shipping");
    			Assert.assertEquals(s,"Total shipping $2.00");
    		}else if(s.contains("TOTAL")){
    			log.info("verifying Total amount");
    			Assert.assertEquals(s,"TOTAL $18.51");
    		}
    		else if(s.contains("Faded Short Sleeve T-shirts")) {
    			
    			log.info("validating product description");
    			Assert.assertTrue(sf.locateElement(order_des).isDisplayed(),"validating product description");
    		}
    		
    	 }
    		
     }
  }
    	
    

